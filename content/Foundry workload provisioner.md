Unicredit wanted to leverage the Foundry skills they already have in-house to develop Data Products and so they needed to interact with it from Witboost. The Foundry workload provisioner was designed to add a workload component to a Data Product which relates to a project area in Foundry where a developer could, then, implement the pipeline they need using tools provided by Palantir.

Palantir Foundry is an enterprise data management PaaS offering tools for working with big data. Palantir Foundry ecosystem is very comprehensive and rather siloed, so integration and being able to communicate with it has been the biggest challange (and, in some cases, that brought limited solutions).

Here is the High Level Design of the component:

![[Palantir HLD.png]]

And here we can see how the provisioning is actually performed end-to-end
![[Pasted image 20240411114030.png]]

## Foundry Workload SP

The Foundry Workload specific provisioner creates a project entity in Foundry and assign all users from the developmentGroup to it with Editor privileges. That said, there are some prerequisites:
- the developmentGroup cannot be empty
- at least one of the members has to be a registered user in Foundry

Aside from basic field checking, the validation phase gets the members of a developmentGroup from Hasura and filters out just the ones that are actually registered on Foundry via a */getUser* endpoint. If none is found, the validation fails.

As for the actual provisioning, it is achieved by triggering a pipeline in Foundry defined by Unicredit to create projects.

![[Pasted image 20240411162421.png]]

Since the user and groups tables are read-only for us, in order to trigger this pipeline we update the *test-upload-json* appending an item to create in the Json file using the */updateDataset* API.

The python code is private and it performs this operations
- creates the projects
- assigns users as Editors (able to do everything on that directory)
- assigns the appropriate marking (e.g. a UCI marking opens to a user only data marked as *uci* (even a single field of a table))
### Critical points

Palantir Foundry exposes some APIs, that's true, though they have not been proven to support our workflow enough; here are some pain points:
- we cannot monitor the provisioning via API, neither see if it has run succesfully or not; the only thing we know is that the pipeline takes around 3 minutes to complete and so the specific provisioner artificially waits 3 minutes before ending the deploy of the Foundry workload
  ![[Pasted image 20240411162753.png]]
  Users have been informed of this behaviour via the template documentation and are invited to check if everything is ok regarding access to the Foundry project
- The Palantir people have not been very collaborative and ignored our needs for a year, acting like they didn't understand them and proposing us to do everything via Palantir tools instead of the integration we were looking for; at the end we managed to get some answers from them and made them put everything written in a document which brought to this v1 of the solution (since priority went down, a v2 wasn't even planned)

### Open points

Only after the delivery of this solution, the Unicredit's Palantir team has said to us that their input dataset for the creation of projects is populated from an online form and after a certain offline approval process, which can take days or weeks. Since there wasn't any interest in this feature at the time, the priority scaled down and I moved to work on other projects; still, I have some considerations about the state of the Foundry Workload:
- the Palantir team presented that process as one for creating data products, while we are just creating projects to support a workload in Foundry (infact we use a different input file on a different branch for the code); for a data product to be developed in Witboost it has to pass a similar approval process, and so I believe we don't have to pass through theirs
- following other Unicredit's processes, we could not automatically provision the data connections from Witboost's output ports and to the Internal GCS storage area of the data product; input or output Data Connections require ad admin user to be performed and Unicredit, which centralize this operation on a dedicated team, wasn't fine to give admin rights to our tech user

# Collaboration with [[Advanced Analytics team]]
