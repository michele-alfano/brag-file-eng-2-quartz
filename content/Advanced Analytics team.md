![[Palantir team 2.png]]

## Roles
Alessio Rosatelli - Foundry developer
Gaetano Secondino - Data Connections
Aldo Martini - PM (later)
Massimiliano Spalluto - PM
Massimo Bricchi - Advanced Analytics chief (?)

## Others

Giulia Pezzino - Foundry developer, consulted for Project creation pipeline