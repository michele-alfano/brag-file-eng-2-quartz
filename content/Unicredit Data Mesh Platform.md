--Sezione su cos'è la platform e come funziona e interazioni (architetturale e funzionale)--

## Data Mesh Platform
Unicredit engaged Agile Lab to install Witboost for the transition of their data pipelines to the Data Mesh.

### My Impact
- Foundry workload provisioner
- Observability API

## Architecture

The implementation for Unicredit follows this architecture.

![[Data Mesh Platform deploymenyt 1.png]]

From the users standpoint, the platform is a collection of services for navigating, accessing and developing their data products.

![[Unicredit platform high level arch.png]]

In this environment, a Data Product follows this lifecycle

![[Dp lifecycle 1.png]]

![[Dp lifecycle 2.png]]

Regarding the development process, the Data Mesh Platform is deployed this way 
![[Pasted image 20240412164053.png]]

In this context I was asked to develop:
- [[Foundry workload provisioner]], a specific provisioner to enable integration with Palantir Foundry
- [[Observability API]], a plugin to let consumers from the platform to access data quality metadata from a proprietary database