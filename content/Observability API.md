This witboost component was part of a bigger requirement asking a dashboard plugin for data products in the marketplace and represented the back-end component of the whole solution.

![[Pasted image 20241120114005.png]]

The goal was to show Data Quality information of data products from a Unicredit's Cloudera-based database system called IGoR.

## Challenge 1

Access to the Cloudera Impala instance, which is deployed on Unicredit premises, cannot be granted to resources deployed on Cloud (this microservice). After a couple of iterations we decided for this design together with Unicredit's internals:

![[Pasted image 20241121115508.png]]

Our team would have been accountable for the left side while Unicredit for the on-prem stuff.

## Challenge 2

While working on the microservice we found another limitation concerning the deployment: Unicredit's policy doesn't allow to access non-production data from production environment.
That should have been fine, except that our Witboost data platform is deployed [this way](obsidian://open?vault=brag-file-eng-2&file=images%2FPasted%20image%2020240412164053.png), with Witboost production environment (here called *promotion*) offering data product development features to users, like of course release management and so managing data from all data product environments.

Even though we explained multiple time our situationto Unicredit's Cloud Eng team, we are still in high waters and so we found out this solution:

![[Observability API definition.png]
![[Observability API LLD.png]]

The gist of it is that if the instance deployed in promotion needs to get quality or development data of a data product, it will behave as a proxy and redirect the call towards the same microservice deployed in the corresponding GCP project that shall be able to access its related Dremio instance.

## Challenge 3

Even though our solution didn't provide authentication , but from the VAPTs (Vulnerability Assessment and Penetration Test) turns out they needed an authentication system. 

## Impact
- Enhanced data quality Confluence documentation
## People

Collaboration with the [[Data Quality Team]] on IGoR side.