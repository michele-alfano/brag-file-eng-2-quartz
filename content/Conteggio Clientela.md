## Domain and Requirements

- Customer
	- clienti acquisiti con perimetro Italia
	- frequenza mensile
	- no retention
	- volume dati sulle migliaia di record (La tabella _h un po' di più ma sempre pochi)

## People
- Nicola Boni - DP owner
- Francesco Mitrozzi - PM (formerly Annunziata Maria Romana)
- Data Modeling Team
	- Elisa Scagliarini
	- Manuela Speranza
	- Loreta del Monte
- Stefano Patti - Data Architect

## Notable Developments
- Data Product
- 2 Dremio OPs

## Impact
