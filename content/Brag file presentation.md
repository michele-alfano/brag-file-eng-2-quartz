---
theme: sky
---


![[Michele Alfano - Brag File#Something about myself]]

![[Michele Alfano - Brag File#Me in Agile Lab]]

---
![[Unicredit Data Mesh Platform#Data Mesh Platform]]

---
## Foundry workload provisioner

- High Level Design
- Low Level Design - Foundry APIs
- Use-case Template
- Tech Adapter

---
## Foundry workload provisioner
### What I learned
- Plugin development for Witboost
- Microservice development
- Clean code and TDD concepts #metrics

### What I offer
- Plugin development for Witboost
- Palantir Foundry APIs knowledge

---
## Observability API

- High Level Design - arch. based on Dremio relay
- Low Level Design - MTLS proxy
- Microservice

---
## Observability API
### What I learned
- Develop a microservice with Java + Springboot
- Apply clean code #metrics
- TLS and MTLS
- AuthN and AuthZ

### What I offer
- Microservice development and deploy
- TLS/MTLS and AuthN/Z knowledge

---
## Data Engineering in Unicredit

![[Unicredit Data Engineering]]

---

### Data Engineering
### What I learned
- End-to-end data product project management
- Airflow scheduler, Pyspark on Dataproc, Dremio workload and OPs
- How to work with XML

### What I offer
- Data product design and development in Witboost 
- Project management
- Accountable communication with customer

---
## Foundry GDPR
### What I learned

- Development of a data pipeline in Palantir Foundry using python
- GDPR concepts: RTBF and Retention
- The importance of domain knowledge

### What I offer

- Palantir Foundry development skill
- Overview of Palantir Foundry ecosystem
---
## Impact in Agile Lab

![[Impact in Agile Lab]]

---
## Impact in Agile Lab
### What I learned
- Effective communcation #metrics
- Training in Agile Lab
- Agile Lab's vision applied

### What I offer
- Effective communication
- Facilitation and presentation skills
- Sponsorship of Agile Lab's vision and strategy