As my first project in Agile Lab I was asked to insert a step in a data pipeline to apply GDPR features.

![[Pasted image 20241119120405.png]]

This project followed a previous implementation by Agile Lab of a gdpr library which offered Unicredit RTBF, Retention and Historicization capabilities to their Foundry pipelines.
![[Pasted image 20241119120637.png]]

-- Code? --
