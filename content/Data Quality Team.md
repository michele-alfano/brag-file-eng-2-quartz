![[Data Quality Team.png]]

## Roles

Giovanno Lombardo - Data Platform & Architecture chief
Silvia Strafella - Data Quality Team chief
Monica Dotto - Data Quality Team
Marina Grivo - Data Quality Team
Developers
- Gabriele Gelli
- Alessia Groppi
Data Governance Team
- Teresa Tropea
- Maria Claudia Riccaboni